# Kunstmaan Bundles Insiders Edition

This edition is based on [Kunstmaan Bundles Standard Edition](https://github.com/Kunstmaan/KunstmaanBundlesStandardEdition)
and installs [Kunstmaan CMS](http://bundles.kunstmaan.be) for Insiders environment.

## Update edition

The [Kunstmaan Bundles Insiders Edition](https://bitbucket.org/insiders/kunstmaan-edition) can be updated by rebasing it on the forked repository ([Kunstmaan Bundles Standard Edition](https://github.com/Kunstmaan/KunstmaanBundlesStandardEdition)).

    git clone git@bitbucket.org:insiders/kunstmaan-edition.git /path/to/kunstmaan-edition
    git remote add --fetch kunstmaan git@github.com:Kunstmaan/KunstmaanBundlesStandardEdition.git
    git rebase kunstmaan/master
    git push --force

## Update project (TODO)

Projects should be updated with the latest changes from [Kunstmaan Bundles Insiders Edition](https://bitbucket.org/insiders/kunstmaan-edition).
I.e. all changes pushed to the edition repository after `composer create-project` has ran for a specific project.
This can be achieved by adding the edition repository as second remote to your project repository

    git clone git@bitbucket.org:insiders/kuma-projectname.git /path/to/kuma-projectname
    git remote add --fetch edition git@bitbucket.org:insiders/kunstmaan-edition.git

You can now create a diff between `origin/master` and `edition/master`.

### Apply generator changes

TODO

## Create new project

- Create a new GIT repository (kuma-projectname)
- Create a new project via composer

        composer create-project -s dev insiders/kunstmaan-edition /path/to/kuma-projectname

- Add the new GIT repostory to the new project

        cd /path/to/kuma-projectname
        git init
        git remote add -m master origin git@bitbucket.org:insiders/kuma-projectname.git
        git add -A .
        git commit -m "Initial project created"
        git push --set-upstream origin master

- Configure deploy settings in `app/config/deploy.rb`
- Configure remote environment parameters in `app/config/deploy/parameters.*.yml`
- Configure static parameters in `app/config/config.yml`
- Optionally disable the multilingual functionality as explained at the bottom of the document
- Push initial project configuration

        git add -A .
        git commit -m "Initial project configuration"
        git push

- Configure phing project in `project.properties`
- Setup database (see `Setup database` chapter)
- Create the kuma website bundle

        phing generate-site

- Dump assets

        app/console --env=prod assetic:dump

- Push initial project installation

        git add -A .
        git commit -m "Initial project installation"
        git push

## Checkout existing project

- Clone the existing GIT repository

        git clone git@bitbucket.org:insiders/kuma-projectname.git /path/to/kuma-projectname

## Install project

- Setup database if necessary (see `Setup database` chapter)
- Build project via phing

        phing install

- Load database fixtures

        app/console doctrine:fixtures:load

# Setup database

During project creation or installation a valid database connection is required, including the application scheme.
Use the credentials as specified in your parameters file.

    cd /path/to/kuma-projectname
    mysql -e "CREATE USER 'username'@'%' IDENTIFIED BY 'password'"
    mysql -e "GRANT ALL PRIVILEGES ON dbname.* TO 'username'@'%'"
    app/console doctrine:database:create
    app/console doctrine:schema:create

# Asset management

Assets come from various sources within the application, being `assetic`, `symfony bundles` and/or `gulp`;

- Assets from _assetic_ and _gulp_ must be available in version control as these are **not** compiled while deploying.
- Assets from _symfony bundles_ are symlinked after `phing install` has ran, these are also symlinked while deploying.

Assets made available via version control **must always** be optimized for production!

## Compilation

Refresh symfony bundle symlinks (new bundles added etc.)

    app/console assets:install --symlink

Recompile assets from assetic

    app/console --env=prod assetic:dump
    # commit changes!

Recompile assets from gulp

    node_modules/gulp/bin/gulp.js build
    # commit changes!

# Disable Multilingual functionality

- Copy `app/config/routing.yml` to `app/config/routing.multilang.yml`
- Copy `app/config/routing.singlelang.yml` to `app/config/routing.yml`
- Copy `app/config/security.yml` to `app/config/security.multilang.yml`
- Copy `app/config/security.singlelang.yml` to `app/config/security.yml`
- Configure `app/config/config.yml`
    - requiredlocales: `locale`
    - defaultlocale: `locale`
    - locale: `locale`
    - multilanguage: false
- Configure `app/config/config_prod.yml`
    - Replace the fos_http_cache match (`path: ^/[^/]+/admin`) with `path: ^/admin`

## Enable domain specific locales

- Set the option `requiredlocales` in `config.yml` to the list of supported locales e.g. `en|nl|de|fr`
- Set the option `multilanguage` in `config.yml` to `true`
- Enable and configure the KunstmaanMultiDomainBundle (https://github.com/Kunstmaan/KunstmaanBundlesCMS/blob/master/docs/05-08-using-the-multi-domain-bundle.md)

# Insiders ODP

To create a Citynavigator™-like project additional configuration and installation steps are required.

## Installation

    cd /path/to/kuma-projectname
    app/console insiders:generate:odp
    # commit changes!

Your `composer.json/lock`, `AppKernel.php` and `routing.yml` should be updated with all ODP dependencies, 
i.e. the bundle is enabled ouf-of-the-box.

## Configuration

This bundle requires minimal configuration for setting up the API connection.

    insiders_odp_client:
        connection:
            hostname: <YOUR_API_HOSTNAME>
            api_key: <YOUR_API_KEY>

The `hostname` element is actually optional as it defaults to `localhost`.

- Show default configuration `app/console config:dump-reference insiders_odp_client`
- Show actual configuration `app/console config:debug insiders_odp_client`

# Search

To use the search functionality additional configuration and installation steps are required.

## Installation

    cd /path/to/kuma-projectname
    app/console insiders:generate:searchpage
    # Commit changes!

If you also want to use the search functionality to search through ODP items you will also need to follow the instructions in the `Insiders ODP` chapter

## Configuration

This bundle requires minimal configuration for setting up the search adapters.

    insiders_kunstmaan_search:
        adapters:
            node:
                view: InsidersKunstmaanSearchBundle:Pages\Search:node.html.twig

If you want to search through ODP items you should add the following configuration

    insiders_kunstmaan_search:
        adapters:
            location:
                view: InsidersOdpClientBundle:Pages\Search:odp.html.twig
            event:
                view: InsidersOdpClientBundle:Pages\Search:odp.html.twig


# Kunstmaan Bundles

This edition uses a fork of kunstmaan/bundles-cms via https://github.com/insiders/KunstmaanBundlesCMS and is
dependend on the insiders branch as configured in composer.json.

## Update master with upstream

- Change working directory:

        cd vendor/kunstmaan/bundles-cms

- Make sure you are on master:

        git checkout master

- Add upstream as remote

        git remote add kunstmaan git@github.com:Kunstmaan/KunstmaanBundlesCMS.git

- Fetch commits from upstream:

        git fetch kunstmaan -p

- Update master:

        git reset --hard kunstmaan/master
        git push origin

- Rebase insiders branch:

        git checkout insiders
        git rebase master
        git push origin

## Create a pull request

- Change working directory:

        cd vendor/kunstmaan/bundles-cms

- Create a new branch:

        git checkout -b bugfix/<name> master

- Make your changes, commit and push it.

- Go to https://github.com/insiders/KunstmaanBundlesCMS/compare/ and compare your branch with upstream master. 

- Use the button "Create pull request" to create a new issue and follow the [Kunstmaan guidelines](https://bundles.kunstmaan.be/documentation/contributing/pull-requests). 
