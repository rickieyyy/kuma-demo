server 'www14.insiders.nl', :app, :web, :primary => true

set :parameters_file,       "app/config/deploy/parameters.production.yml"
set :branch,                "master"
