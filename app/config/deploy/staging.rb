server 'staging3.insiders.nl', :app, :web, :primary => true

set :parameters_file,       "app/config/deploy/parameters.staging.yml"
set :branch,                "master"
