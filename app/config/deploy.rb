# application config
set :application,    "kunstmaan-edition"
set :domain,         "www.kuma-demo.nl"
set :deploy_to,      "/var/www/sites/kuma-demo/www"
set :deploy_via,     :remote_cache
set :keep_releases,  3
set :app_path,       "app"
set :web_path,       "web"
set :asset_children, []
set :repository,     "git@bitbucket.org:rickieyyy/kuma-demo.git"
set :scm,            :git
set :git_enable_submodules, 0
set :model_manager, "doctrine"
set :interactive_mode, false

# shared files
set :shared_children,       ["app/logs", "app/sessions", "web/uploads"]

# composer specific
set :composer_bin,          "/usr/local/bin/composer"
set :use_composer,          true
set :copy_vendors,          true

# assets
set :assets_install,        true
set :assets_symlinks,       true
set :assets_relative,       true

# permissions
set :writable_dirs,         ["app/cache", "app/logs", "app/sessions", "web/uploads"]
set :permission_method,     :acl
set :use_set_permissions,   true

# ssh configuration
set :user,                  "deploy"
set :password,              "rjhkHgjQSaM7"
set :use_sudo,              false
ssh_options[:forward_agent] = true
default_run_options[:pty]   = true

# Be more verbose by uncommenting the following line
#logger.level = Logger::MAX_LEVEL


namespace :insiders do

    desc "Upload parameters.yml"
    task :upload_parameters do
      if exists?(:parameters_file) then
        destination_file = latest_release + "/app/config/parameters.yml"
        top.upload parameters_file, destination_file, :via => :scp
      end
    end

    desc "Creates insiders specific symlinks"
    task :create_symlink, :roles => :app do
      if web_path != "public" then
        puts "--> Symlinking web directory!".green
        run "ln -nfs #{web_path} #{latest_release}/public"
      end
    end

    desc "Restarts the web server"
    task :apache_reload, :roles => :app do
        puts "--> Reload apache!".green
        sudo "/usr/sbin/apachectl graceful"
    end

    desc "Clear varnish cache"
    task :varnish_clear, :roles => :app, :except => { :no_release => true }, :on_error => :continue do
        puts "--> Flush varnish on #{domain}".green
        sudo "varnishadm -S /etc/varnish/secret -T localhost:6082 ban \"req.http.host ~ \"#{domain}\"\""
    end

    desc "Set permissions"
    task :set_permissions, :roles => :app, :except => { :no_release => true } do
        capifony_pretty_print "--> Setting group ownership"
        writable_dirs.each do |link|
            if shared_children && shared_children.include?(link)
              absolute_link = shared_path + "/" + link
            else
              absolute_link = latest_release + "/" + link
            end

            run "chmod 2775 #{absolute_link}"
            run "chgrp upload #{absolute_link}"
        end
        capifony_puts_ok
    end

end

after "deploy:set_permissions", "insiders:set_permissions" # Set permissions
after "deploy:create_symlink", "deploy:cleanup"            # Automatically cleanup old releases
after "deploy:create_symlink", "insiders:create_symlink"   # Insiders specific symylinks
after "deploy:create_symlink", "insiders:apache_reload"    # Flush realpath cache
after "deploy:create_symlink", "insiders:varnish_clear"    # Flush varnish cache
after "deploy:rollback", "insiders:apache_reload"          # Flush realpath cache
after "deploy:rollback", "insiders:varnish_clear"          # Flush varnish cache
after "deploy:share_childs", "insiders:upload_parameters"  # Upload parameters
