var demoWebsiteBundle = demoWebsiteBundle || {};

demoWebsiteBundle.app = (function($, window, undefined) {
    var init;

    init = function() {
        demoWebsiteBundle.nav.init();
    };

    return {
        init: init
    };

}(jQuery, window));

$(function() {
    demoWebsiteBundle.app.init();
});
