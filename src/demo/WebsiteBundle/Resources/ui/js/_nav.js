var demoWebsiteBundle = demoWebsiteBundle || {};

demoWebsiteBundle.nav = (function(window, undefined) {

    var init, floatBreakpointChange;

    init = function() {
        $(window).on('resize', floatBreakpointChange);
        $(window).on('load', floatBreakpointChange);

        $('.handles__item--navigation-open .handles__link').click(function(e) {
            e.preventDefault();
            $('header').toggleClass('navigation-is-open navigation-is-closed');
            $('.main-header__nav').toggleClass('is-open is-closed');
        });

        $('.handles__item--navigation-close .handles__link').click(function(e) {
            e.preventDefault();
            $('header').toggleClass('navigation-is-open navigation-is-closed');
            $('.main-header__nav').toggleClass('is-open is-closed');
        });

        $('.language-nav__active-lang').click(function() {
            $('.language-nav').toggleClass('language-nav--is-open language-nav--is-closed');
        });
    };

    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    floatBreakpointChange = debounce(function() {
        // detect wether the viewport is equal/larger than the float breakpoint (desktop), or smaller (tablet/mobile)
        if ($('.main-nav').css('position') === 'fixed' || $('.main-nav').css('position') === 'absolute') {
            // Mobile menu:
            // Move language-nav and social-nav to main-header__nav container
            $('.main-header__nav').append($('.language-nav'));
            $('.main-header__nav').append($('header .social-nav'));
        } else {
            // Desktop menu:
            // Move language-nav and social-nav back to header
            $('.main-header__logo').after($('header .social-nav'));
            $('.main-header__logo').after($('.language-nav'));
            // Set classes
            $('header').removeClass('navigation-is-open').addClass('navigation-is-closed');
            $('.main-header__nav').removeClass('is-open').addClass('is-closed');
        }
    }, 250);

    return {
        init: init
    };

}(window));
